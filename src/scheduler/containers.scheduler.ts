import { Scheduler } from '@kominal/lib-node-scheduler';
import Docker from 'dockerode';
import { mqttClient } from '..';
import { ENVIRONMENT_NAME, NODE_HOSTNAME, OBSERVER_TENANT_ID, PROJECT_NAME } from '../helper/environment';

export class ContainersScheduler extends Scheduler {
	private docker = new Docker();

	async run(): Promise<void> {
		const info = await this.docker.info();

		let globalCpu = 0;
		let globalMemory = 0;

		const containers = await this.docker.listContainers();
		for (const container of containers) {
			const containerObject = this.docker.getContainer(container.Id);
			const containerInspect = await containerObject.inspect();
			const stats = await containerObject.stats({ stream: false });

			const cpuDelta = stats.cpu_stats.cpu_usage.total_usage - stats.precpu_stats.cpu_usage.total_usage;
			const systemCpuDelta = stats.cpu_stats.system_cpu_usage - stats.precpu_stats.system_cpu_usage;
			const numberOfCpus = stats.cpu_stats.online_cpus;

			const cpu = Math.round((cpuDelta / systemCpuDelta) * numberOfCpus * 100);

			const usedMemory = stats.memory_stats.usage - stats.memory_stats.stats.cache;
			const availableMemory = stats.memory_stats.limit;

			const memory = Math.round((usedMemory / availableMemory) * 100);

			console.log(memory, cpu);
			globalCpu += cpu;
			globalMemory += memory;

			//console.log(containerInspect.Config.Env, stats.memory_stats, stats.cpu_stats);
		}

		await mqttClient.publish(
			`${OBSERVER_TENANT_ID}/${PROJECT_NAME}/${ENVIRONMENT_NAME}/${NODE_HOSTNAME}/0/event`,
			JSON.stringify({
				time: new Date(),
				type: 'USAGE',
				content: { cpu: globalCpu, memory: globalMemory },
			})
		);
	}
}
